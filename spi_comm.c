/*
 * The Ultislave Firmware
 * This file is responsible for communication over the SPI bus.
 * Copyright 2014 (c) by Olliver Schinagl <o.schinagl@ultimaker.com>
 * All rights reserved.
 *
 *
 * This file is part of the Ultislave firmware.
 *
 * Ultislave is free software: you can redistribute it and/or modify
 * it under the terms of the Affero GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Ultislave is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero GNU General Public License for more details.
 * 
 * You should have received a copy of the Affero GNU General Public License
 * along with Ultislave.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>
#include <util/atomic.h>

#include "bitops.h"

#include "spi_comm.h"

#define SS _BV(PB0)
#define SCLK _BV(PB1)
#define MOSI _BV(PB2)
#define MISO _BV(PB3)
#define DDR_SPI DDRB
#define SPI_PORT PORTB

#define SPI_DATA_BUFFER_SIZE 8


static volatile uint8_t spi_data_in[SPI_DATA_BUFFER_SIZE];
static volatile uint8_t spi_data_out;
static volatile uint8_t spi_data_cnt;

void spi_init_slave(uint8_t initial_out)
{
	/* Set MISO as output */
	bit_set(DDR_SPI, MISO);
	/* Enable SPI, SPI irq, Disable data ord and set as slave */
	SPCR = _BV(SPE) | _BV(SPIE);

	SPDR = initial_out; /* Make sure first transmitted byte is not random */
}

ISR(SPI_STC_vect, ISR_BLOCK)
{
	SPDR = spi_data_out;
	spi_data_in[spi_data_cnt] = SPDR;
	spi_data_cnt = (spi_data_cnt + 1) % SPI_DATA_BUFFER_SIZE;
}

/* This function is expected to be called often enough so that the data_in
 * buffer does not overrun. Data_in may contain up to SPI_DATA_BUFFER_SIZE data
 * bytes and this function will process and return one after each call.
 *
 */
int8_t spi_tranceive(uint8_t *data_in, uint8_t data_out)
{
	static uint8_t data_cnt_local;
	static uint8_t data_in_local;
	static uint8_t data_cnt_processed;
	int retval;

	retval = 0;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		spi_data_out = data_out;
		data_cnt_local = spi_data_cnt;
	}
	if (data_cnt_processed != data_cnt_local) {
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			data_in_local = spi_data_in[data_cnt_processed];
		}
		data_cnt_processed = (data_cnt_processed + 1) % SPI_DATA_BUFFER_SIZE;
		*data_in = data_in_local;

		retval = 1;
	}

	return retval;
}
