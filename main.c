#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>

#define SET_BIT(pn, prt)	((prt) |= (pn))
#define CLEAR_BIT(pn, prt)	((prt) &= ~(pn))
#define CHANGE_BIT(pn, prt)	((prt) ^= (pn))

#define SS _BV(PB0)
#define SCLK _BV(PB1)
#define MOSI _BV(PB2)
#define MISO _BV(PB3)
#define DDR_SPI DDRB
#define SPI_PORT PORTB

#define RX_LED _BV(PD4)
#define TX_LED _BV(PD5)
#define TEST_LEAD _BV(PD6)
#define DDR_LED DDRD
#define PORT_LED PORTD

#define FALSE 0
#define TRUE (!FALSE)

#define ACK 0x7e


void spi_init_master(void)
{
	/* Set MOSI, SCLK, SS as output */
	SET_BIT(MOSI, DDRB);
	SET_BIT(SCLK, DDRB);
	SET_BIT(SS, DDRB);
	SET_BIT(SS, PORTB); /* Set SS high */

	/* Set SPI as master */
	SET_BIT(MSTR, SPCR);
	/* Set Prescaler as F_CPU / 16 */
	SET_BIT(SPR0, SPCR);
	/* Enable SPI interrupts */
//	SET_BIT(SPIE, SPCR);
	/* Enable SPI */
	SET_BIT(SPE, SPCR);
}

void spi_init_slave(void)
{
	/* Set MISO as output */
	SET_BIT(MISO, DDR_SPI);
	CLEAR_BIT(MOSI, DDR_SPI);
	CLEAR_BIT(SS, DDR_SPI);
	CLEAR_BIT(SCLK, DDR_SPI);
	SET_BIT(MOSI, SPI_PORT);
	SET_BIT(SS, SPI_PORT);
	SET_BIT(SCLK, SPI_PORT);
	
	/* Enable SPI */
	SPCR = SPE;
}

void led_on(uint8_t pin)
{
	CLEAR_BIT(pin, PORT_LED);
}

void led_off(uint8_t pin)
{
	SET_BIT(pin, PORT_LED);
}

uint8_t spi_transceive(uint8_t data)
{
	uint8_t toggle = 0;

	SPDR = data;
	while (!(SPSR & (1 << SPIF))) {

		if (toggle) {
			CLEAR_BIT(MISO, SPI_PORT);
			led_off(TEST_LEAD);
		} 
		else
		{
			SET_BIT(MISO, SPI_PORT);
			led_on(TEST_LEAD);
		}

		toggle = !toggle;
	};

	return SPDR;
}

int main(void)
{
	uint8_t data_in, data_out;

	/* Enable Global interupts */
	sei();

	/* Configure as outputs */
	SET_BIT(RX_LED, DDR_LED);
	SET_BIT(TX_LED, DDR_LED);
	SET_BIT(TEST_LEAD, DDR_LED);

	spi_init_slave();

	led_off(RX_LED);
	led_off(TX_LED);
	led_off(TEST_LEAD);
	_delay_ms(1000);
	led_on(RX_LED);
	_delay_ms(1000);
	led_on(TEST_LEAD);
	led_on(TX_LED);
	_delay_ms(1000);
	led_off(RX_LED);
	_delay_ms(1000);
	led_off(TX_LED);
	led_off(TEST_LEAD);


	while(TRUE) {
		data_out = 0x01;
		data_in = spi_transceive(data_out++);
		if (data_in == 0x10) {
			led_on(RX_LED);
		}
		if (data_in == 0x20) {
			led_off(RX_LED);
		}
		if (data_in == 0x30) {
			led_on(TX_LED);
		}
		if (data_in == 0x40) {
			led_off(TX_LED);
		}
		_delay_ms(250);
		led_on(TX_LED);
		_delay_ms(250);
		led_off(TX_LED);
	}

	return 0;
}
