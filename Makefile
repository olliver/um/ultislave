DEVICE = atmega16u2
CLOCK = 16000000
PROGRAMMER = -c buspirate -P /dev/ttyUSB0
SOURCES = ultislave.c spi_comm.c spi_comm.h
OBJECTS = ultislave.o spi_comm.o

AVRDUDE = avrdude $(PROGRAMMER) -p $(DEVICE)
CC = avr-gcc -Wall -std=gnu99 -O2 -DF_CPU=$(CLOCK) -mmcu=$(DEVICE)

all: ultislave.hex

.c.o:
	$(CC) -c $< -o $@

.S.o:
	$(CC) -S $< -o $@

.c.s:
	$(CC) -S $< -o $@

commit: all
	git add ${SOURCES}
	git commit -m 'compile commit'

flash: all
	$(AVRDUDE) -U flash:w:ultislave.hex:i

clean:
	rm -f ultislave.hex ultislave.elf $(OBJECTS)

ultislave.elf: $(OBJECTS)
	$(CC) -o ultislave.elf $(OBJECTS)

ultislave.hex: ultislave.elf
	rm -f ultislave.hex
	avr-objcopy -j .text -j .data -O ihex ultislave.elf ultislave.hex
	avr-size --format=avr --mcu=$(DEVICE) ultislave.elf

disasm: ultislave.elf
	avr-objdump -d ultislave.elf

cc:
	$(CC) -E ultislave.c
