#include<avr/io.h>
#include<util/delay.h>
#include <avr/interrupt.h>

#define bit_set(pn, prt)	((prt) |= (pn))
#define bit_clear(pn, prt)	((prt) &= ~(pn))
#define bit_flip(pn, prt)	((prt) ^= (pn))

#define DD_MISO PB3
#define DD_MOSI PB2
#define DD_SCK PB1
#define DDR_SPI DDRB

void SPI_SlaveInit(void)
{
	// Set MISO output, all others input
	DDR_SPI = (1<<DD_MISO);
	// Enable SPI
	SPCR = (1<<SPE);
}

char SPI_SlaveReceive(void)
{
	// Wait for reception complete
	// SPI Status Reg & 1<<SPI Interrupt Flag
	while(!(SPSR & (1<<SPIF)));

	// Return data register
	return SPDR;
}

int main()
{
	SPI_SlaveInit();
	DDRD=255; //PORTC as output,for testing
	

	while(1)
	{
		SPI_SlaveInit();
		PORTD=SPI_SlaveReceive();
	}
	return 0;
}

