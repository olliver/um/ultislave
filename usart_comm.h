/*
 * The Ultislave Firmware
 * This file is responsible for communication over the USART bus.
 * Copyright 2014 (c) by Olliver Schinagl <o.schinagl@ultimaker.com>
 * All rights reserved.
 *
 *
 * This file is part of the Ultislave firmware.
 *
 * Ultislave is free software: you can redistribute it and/or modify
 * it under the terms of the Affero GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Ultislave is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero GNU General Public License for more details.
 * 
 * You should have received a copy of the Affero GNU General Public License
 * along with Ultislave.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef _USART_COMM_H
#define _USART_COMM_H

#include <stdint.h>




#endif /* _USART_COMM_H */
