#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "bitops.h"

#include "spi_comm.h"
#include "usart_comm.h"

#define RX_LED _BV(PD4)
#define TX_LED _BV(PD5)
#define TEST_LEAD _BV(PD6)
#define DDR_LED DDRD
#define PORT_LED PORTD


int main(void)
{
	DDRD = 0xff; //PORTC as output,for testing

	spi_init_slave(0x88);
	sei();

	while (TRUE) {
		uint8_t data_in, data_out;

		data_out = 0x88;

		if (spi_tranceive(&data_in, data_out)) {
			/* do something with the received data */
			PORTD = data_in;
		}
	}

	return 0;
}
